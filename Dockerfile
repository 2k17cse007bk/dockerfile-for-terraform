FROM amazonlinux
RUN yum -y update && yum -y upgrade
RUN yum install -y yum-utils sudo
RUN sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
RUN sudo yum -y install terraform
RUN mkdir terraform
RUN wget https://bk2k17-shell.s3.amazonaws.com/create-ec2-using-tf.tf
COPY ./create-ec2-using-tf.tf /terraform/
RUN cd terraform
WORKDIR /terraform
RUN terraform init
RUN terraform apply -auto-approve

